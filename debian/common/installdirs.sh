#!/bin/sh

set -e

if [ -z "$1" ]
  then
    echo "please specify the build directory to create"
    exit 1
fi

install -d debian/$1

if [ -f debian/dirs ]
  then
    cat debian/dirs | while read line
      do
        install -d debian/"$1"/"$line"
      done
fi
